(** Zadanie   : Sortowanie topologiczne **)
(** Autor     : Andrzej Swatowski       **)
(** Recenzent : Krzysztof Piesiewicz    **)

open PMap

(* Rozwiązanie opiera się na tak zwanym Algorytmie Kahna:
   1. Tworzymy "wór" z wierzchołkami grafu o stopniu wejścia równym 0
   2. Wyciągając wierzchołek z wora wypisujemy go oraz zmniejszamy jego sąsiadom
      stopień wejścia o 1. Jeżeli stopień wejścia któregoś z sąsiadów będzie
      wynosić zero, to dodajemy go do wora.
   3. By sprawdzić, czy nie ma cyklu wystarczy sprawdzić stopnie wejścia
      wszystkich wierzchołków, gdy wór będzie już pusty. Jeżeli któryś stopień
      jest nierówny 0, to znaczy, że w grafie musi być cykl.                  *)

exception Cykliczne

let rec create_map l m =
  let rec neighbours m = function
  | [] -> m
  | h::t ->
      if not (mem h m) then
        neighbours (add h (1, []) m) t
      else
        let (indeg, h_adj) = find h m in
        neighbours (add h ((indeg + 1), h_adj) m) t
  in
  match l with
  | [] -> m
  | (node, adj)::t ->
      let new_map = neighbours m adj in
      if not (mem node new_map) then
        create_map t (add node (0, adj) new_map)
      else
        let (indeg, _) = find node new_map in
        create_map t (add node (indeg, adj) new_map)

let build_sack m =
  foldi (fun node (indeg, _) acc -> if indeg = 0 then node::acc else acc) m []

let rec traverse s (m, out) =
  let rec neighbours (m, s) = function
  | [] -> (m, s)
  | h::t ->
      let (indeg, h_adj) = find h m in
      if indeg - 1 = 0 then
        neighbours ((add h ((indeg - 1), h_adj) m), h::s) t
      else
        neighbours ((add h ((indeg - 1), h_adj) m), s) t
  in
  match s with
  | [] -> (m, out)
  | h::t ->
      let (_, adj) = find h m in
      let (m, s) = neighbours (m, t) adj in
      traverse s (m, h::out)

let cycle m =
  fold (fun (indeg, _) b -> not (indeg = 0) || b = true) m false;;

let topol l =
  let gen_map = create_map l empty in
  let (processed_list, out) = traverse (build_sack gen_map) (gen_map, []) in
  if cycle processed_list then
    raise Cykliczne
  else
    List.rev out

(* TESTY *)
(*
let l1 = [(0, [1; 2; 3]); (2, [3; 5; 8]); (3, [4; 5]); (1, [4; 5]); (6, [7; 8]);
          (9, [0])]
let l2 = [("czapka", ["plaszcz"]); ("sweter", ["koszula"]);
          ("spodnie", ["koszula"; "skarpety"; "bokserki"]);
          ("marynarka", ["sweter"]); ("szal", ["marynarka"]);
          ("poszetka", ["marynarka"]);
          ("plaszcz", ["szal"; "spodnie"; "marynarka"; "poszetka"])]
let l3 = [(0, [2]); (1, [0; 2]); (3, [0; 1; 4]); (4, [2; 1]); (5, [0; 4])]

let map1 = create_map l1 empty
let map2 = create_map l2 empty

let sack1 = build_sack map1;;
let sack2 = build_sack map2;;
let trav1 = traverse sack1 (map1, []);;
let trav2 = traverse sack2 (map2, []);;

let outlist = topol l1
let outlist2 = List.rev topol l2
let outlist3 = topol l3

let l = [(2, [5]); (5, [7]); (3, [5]); (7, [3])];;
try topol l with | Cykliczne -> Printf.printf "Znaleziono cykl!\n"; []
let l = [(1, [1])];;
try topol l with | Cykliczne -> Printf.printf "Znaleziono cykl!\n"; []

(* Testy Krzysztofa Piesiewicza *)
let l = [ (1, [4]); (2, [4; 5; 20]); (3, [5]); (4, [6]); (5, [4; 7; 8]);
(6, [9]); (7, [10; 11]); (8, [12]); (9, []); (10, []); (11, []); (12, []);
(13, []); (14, [1]); (15, [14; 2; 18; 19]); (16, [15]); (17, [15]) ];;
topol l;;

let l = [ (1, [4]); (2, [4; 5; 20]); (3, [5]); (4, [6]); (5, [4; 7; 8]);
(6, [9]); (7, [10; 11]); (8, [12; 17]); (9, []); (10, []); (11, []); (12, []);
(13, []); (14, [1]); (15, [14; 2; 18; 19]); (16, [15]); (17, [15]) ];;
try topol l with | Cykliczne -> Printf.printf "Znaleziono cykl!\n"; [];;
*)

