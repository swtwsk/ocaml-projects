(** Zadanie   : Przelewanka      **)
(** Autor     : Andrzej Swatowski**)
(** Recenzent : Amelia Rossowska **)

let nwd x y =
  let rec euklides x y =
    if y = 0 then x else euklides y (x mod y)
  in
  if x >= 0 then euklides x y else euklides y x

let przelewanka a =
  let wynik = ref (-1) and znaleziono = ref false in
  let n = Array.length a in
  let poczatek = Array.make n 0
  and koniec = Array.map snd a
  and pojemnosc = Array.map fst a
  in
(*Przypadki brzegowe - dla pustej tablicy otrzymujemy zero wykonanych ruchów;
  dla niepustej tablicy warto zauważyć, że każdy ruch doprowadza do sytuacji,
  gdy co najmniej jedna ze szklanek jest pusta lub pełna; prócz tego
  z rozszerzonego algorytmu Euklidesa (bądź tożsamości Bézouta) wynika, że jeśli
  w stanie wynikowym którakolwiek zadana ilość wody nie dzieli się przez NWD
  pojemności wszystkich szklanek, to nie można jej uzyskać - jeżeli więc zadany
  stan końcowy nie spełnia tych wymogów, to nie jest możliwy do uzyskania *)
  let przypadki_brzegowe =
    let nwd_poj = Array.fold_left nwd 0 pojemnosc in
    if not (Array.fold_left (fun acc x ->
                               acc || (snd x = 0) || (fst x = snd x)) false a)
    then true
    else
      if nwd_poj <> 0 then
        if Array.fold_left (fun a x -> a && (x mod nwd_poj <> 0)) true koniec
        then true
        else false
      else false
  in

  if n = 0 then 0
  else if przypadki_brzegowe then (-1)
  else
  (*Kolejka q służy do przejścia BFS po drzewie "stanów",
    zaś tablica z haszowaniem h kojarzy pary (stan -> ilość ruchów) *)
    let q = Queue.create () and h = Hashtbl.create n in
    let hash_add nowy odl =
      if nowy = koniec then begin
        wynik := odl + 1;
        znaleziono := true
      end else if not (Hashtbl.mem h nowy) then begin
        Hashtbl.add h nowy (odl + 1);
        Queue.push nowy q
      end else if Hashtbl.find h nowy > odl + 1 then begin
        Hashtbl.add h nowy (odl + 1);
        Queue.push nowy q
      end
    in

    let nalej stan odl =
      let f i st =
        if st <> pojemnosc.(i) then begin
          let nowy_stan = Array.copy stan in
          nowy_stan.(i) <- pojemnosc.(i);
          hash_add nowy_stan odl
        end
      in
      Array.iteri f stan
    
    and przelej stan odl =
      let f i sti =
        if sti <> 0 then
          Array.iteri (fun j stj ->
                         if i <> j && stj <> pojemnosc.(j) then begin
                           let nowy_stan = Array.copy stan in
                           let wlane = min pojemnosc.(j) (sti + stj) in
                           let wylane = sti - (wlane - stj) in
                           nowy_stan.(i) <- wylane;
                           nowy_stan.(j) <- wlane;
                           hash_add nowy_stan odl
                         end) stan
      in
      Array.iteri f stan

    and wylej stan odl =
      let f i st =
        if st <> 0 then begin
          let nowy_stan = Array.copy stan in
          nowy_stan.(i) <- 0;
          hash_add nowy_stan odl
        end
      in
      Array.iteri f stan
    in

  (*Właściwe przejście BFS, poprzez budowanie drzewa możliwych stanów. Jeżeli
    program trafi na stan, który jest równoważny wynikowemu, to kończy
    wykonywanie pętli - przechodzimy bowiem drzewo BFSem, więc pierwszy poprawny
    wynik jest zarazem wynikiem najlepszym (każdy kolejny jest na tym samym
    poziomie lub głębiej) *)
    Queue.push poczatek q;
    Hashtbl.add h poczatek 0;
    while not (Queue.is_empty q) && not !znaleziono do
      let akt_stan = Queue.pop q in
      let akt_odl = Hashtbl.find h akt_stan in
      if akt_stan = koniec then begin
        wynik := akt_odl;
        znaleziono := true
      end else begin
        nalej akt_stan akt_odl;
        przelej akt_stan akt_odl;
        wylej akt_stan akt_odl
      end
    done;
    !wynik

(** TESTY **)
(*
let () = print_endline "TESTY"
let () = assert (przelewanka [| (10, 3); (5, 5) |] = -1);
         print_endline "NWD ok"
let () = assert (przelewanka [| (5, 3); (3, 2); (3, 2) |] = -1);
         print_endline "Niepuste/niepelne ok"
let () = assert (przelewanka [| |] = 0);
         assert (przelewanka [| (0, 0); (0, 0) |] = 0);
         assert (przelewanka [| (2, 0); (1000, 0); (1000500100900, 0) |] = 0);
         assert (przelewanka [| (5, 3); (1, 0) |] = 5);
         assert (przelewanka [| (5, 2); (1, 0) |] = 4);
         assert (przelewanka [| (3, 0); (4, 1); (3, 1) |] = 7);
         assert (przelewanka [| (3, 0); (4, 3); (3, 3) |] = 3);
         assert (przelewanka [| (3, 0); (4, 1); (3, 0) |] = 3);
         assert (przelewanka [| (3, 0); (4, 1); (3, 3) |] = 2);
         print_endline "Testy OK"

(* Testy z Facebooka *)
let () = assert(przelewanka [|(5, 0); (3, 0); (1, 0) |] = 0);
         assert(przelewanka [|(5, 5); (3, 3); (1, 1) |] = 3);
         assert(przelewanka [|(5, 3); (3, 3); (1, 0) |] = 3);
         assert(przelewanka [|(5, 2); (3, 2); (1, 0) |] = 4);
         assert(przelewanka [|(5, 1); (3, 1); (7, 1) |] = -1);
         assert(przelewanka [|(5, 1); (3, 1); (7, 0) |] = 7);
         assert(przelewanka [|(5,1); (3, 1); (7, 7) |] = 7);
         assert(przelewanka [|(5,0); (3, 1); (7, 1) |] = 6);
         assert(przelewanka [|(5,1); (3, 0); (7, 1) |] = 7);
         assert(przelewanka [|(5,5); (3, 1); (7, 1) |] = 5);
         assert(przelewanka [|(5,1); (3, 0); (7, 1) |] = 7);
         assert(przelewanka [|(5,3); (3, 3); (7, 1) |] = 4);
         assert(przelewanka [|(5,3); (3, 0); (7, 4) |] = 3);
         assert(przelewanka [|(5,0); (3, 3); (7, 4) |] = 2);
         assert(przelewanka [|(5,0); (3, 0); (7, 7) |] = 1);
         print_endline "Testy Facebook OK"
*)
