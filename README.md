# OCaml projects

Projects made for Introduction to Programming (Functional approach) classes at University of Warsaw

### Disclaimer

We had to write most of our codes in polish language

## Projects descriptions 

### Arytmetyka (Arithmetic)

A library of arithmetic operations on intervals (for handling of measurement errors)

### Drzewa lewicowe (Leftist Trees)

Priority queue built using leftist tree data structure.

### Origami

A library counting how many layers of folded paper sheet is in certain point.

### Modyfikacja drzew (Modification of Trees)

Modification of given implementation of AVL trees, holding intervals instead of single elements.

### Sortowanie topologiczne (Topological sorting)

Implementation of topological sorting algorithm.

### Przelewanka (Spilling)

```
Given n empty glasses with capacities x1, x2, ..., xn find minimal number of steps needed to fill glasses with given amounts y1, y2, ..., yn.
A problem is represented by an array: [|(x1,y1); (x2,y2); ...; (xn,yn)|].
You may only: 
- fullfil the chosen glass
- empty the chosen glass
- spill water from one glass to another
```

### Dijkstra

Simple implementation of Dijkstra algorithm using priority queue written in *Leftist Tree*.
