(** Zadanie   : Modyfikacja drzew **)
(** Autor     : Andrzej Swatowski **)
(** Recenzent : Kamila Winnicka   **)

(*
 * ISet - Interval sets
 * Copyright (C) 1996-2016 Xavier Leroy, Nicolas Cannasse, Markus Mottl,
 * Jacek Chrzaszcz, Andrzej Swatowski
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version,
 * with the special exception on linking described in file LICENSE.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

let abs x =
  if x = min_int then max_int else abs x

type interval = int * int

(* typ t to albo puste drzewo, albo węzeł postaci krotki
(lewy syn, przedział, prawy syn, wysokość drzewa, ilość elementów w drzewie) *)
type t =
  | Empty
  | Node of t * interval * t * int * int

(* typ merge dla funkcji, która zwraca połączone przedziały                  *)
type merge =
  | NullInt
  | Interval of interval

let height = function
  | Node (_, _, _, h, _) -> h
  | Empty -> 0

let w = function
  | Node (_, _, _, _, w) -> w
  | Empty -> 0

(* komparator sprawdza, czy podana liczba znajduje się w zadanym przedziale  *)
let split_cmp a b x =
  if x <= b then
    if x >= a then 0 else -1
  else 1

let amount (a, b) =
  if b - a + 1 <= 0 then                         (* przekroczenie max_int !  *)
      max_int
    else
      b - a + 1

(* funkcje porównujące z przypadkiem min_int *)
let less a b =
  if b = min_int then a < b
  else a < b - 1

let less_equal a b =
  if b = min_int then a <= b
  else a <= b - 1

let make l k r =
  Node (l, k, r, max (height l) (height r) + 1, (w l) + (w r) + (amount k))

let empty = Empty

let is_empty s =
  s = Empty

let bal l k r =
  let hl = height l in
  let hr = height r in
  if hl > hr + 2 then
    match l with
    | Node (ll, lk, lr, _, _) ->
        if height ll >= height lr then make ll lk (make lr k r)
        else
          (match lr with
          | Node (lrl, lrk, lrr, _, _) ->
              make (make ll lk lrl) lrk (make lrr k r)
          | Empty -> assert false)
    | Empty -> assert false
  else
    if hr > hl + 2 then
      match r with
      | Node (rl, rk, rr, _, _) ->
          if height rr >= height rl then make (make l k rl) rk rr
          else
            (match rl with
            | Node (rll, rlk, rlr, _, _) ->
                make (make l k rll) rlk (make rlr rk rr)
            | Empty -> assert false)
      | Empty -> assert false
    else
      Node (l, k, r, max hl hr + 1, (w l) + (w r) + (amount k))

(* funkcja merge służy do łączenia ze sobą przedziałów, zwracająć czwórkę
   (lewy interwał, prawy interwał, bool lewy, bool prawy), gdzie wartości
   logiczne informują, czy przedział w zadanym wierzchołku został rozszerzony*)
let int_merge (a, b) (c, d) =
  let cmp x y = 0 <= abs (x - y) && abs (x - y) <= 1 in
  if less_equal a c then
    if less_equal b c then
      if cmp b c then
        (NullInt, (a, d), true, false)
      else
        (Interval (a, b), (c, d), true, false)
    else
      if less_equal b d then
        (NullInt, (a, d), true, false)
      else
        (NullInt, (a, b), true, true)
  else
    if less_equal d a then
      if cmp d a then
        (NullInt, (c, b), false, true)
      else
        (Interval (c, d), (a, b), false, true)
    else
      if less_equal d b then
        (NullInt, (c, b), false, true)
      else
        (NullInt, (c, d), false, false)

(* funkcje "zwijające" drzewo usuwają węzły, których przedziały się przecinają
i zwracają dwójkę postaci:
((nowe poddrzewo), (wartość, o którą trzeba rozszerzyć przedział w korzeniu)) *)
let rec fold_left x = function
  | Empty -> (Empty, x)
  | Node (l, (lk, rk), r, _, _) ->
      if less x lk then
        fold_left x l
      else
        if less rk x then
          let (folded_tree, v) = fold_left x r in
          (make l (lk, rk) folded_tree, v)
        else
          (l, lk)

let rec fold_right y = function
  | Empty -> (Empty, y)
  | Node (l, (lk, rk), r, _, _) ->
      if less rk y then
        fold_right y r
      else
        if less y lk then
          let (folded_tree, v) = fold_right y l in
          (make folded_tree (lk, rk) r, v)
        else
          (r, rk)

(* funkcja add_one umieszcza przedział w drzewie i sprawdza, czy nowoumieszczony
   przedział nie przecina się z którymś z istniejących wewnątrz poddrzew *)
let rec add_one i = function
  | Node (l, k, r, _, _) ->
      let (lk, rk) = k and (x, y) = i in
      if less rk x then
        let nr = add_one i r in
        bal l k nr
      else if less y lk then
        let nl = add_one i l in
        bal nl k r
      else
        let (intv, (nlk, nrk), left, right) = int_merge i k in
        (match intv with
         | NullInt ->
             if left then
               if right then
                 let (l, nlk) = fold_left x l in
                 let (r, nrk) = fold_right y r in
                 bal l (nlk, nrk) r
               else
                 let (l, nlk) = fold_left x l in
                 bal l (nlk, nrk) r
             else
               if right then
                 let (r, nrk) = fold_right y r in
                 bal l (nlk, nrk) r
               else
                 bal l (nlk, nrk) r
         | Interval (a, b) ->
             if left then
               bal (add_one (a, b) l) (nlk, nrk) r
             else if right then
               bal l (a, b) (add_one (nlk, nrk) r)
             else assert false)
  | Empty -> Node (Empty, i, Empty, 1, amount i)

let rec join l v r =
  match (l, r) with
    | (Empty, _) -> add_one v r
    | (_, Empty) -> add_one v l
    | (Node(ll, lv, lr, lh, _), Node(rl, rv, rr, rh, _)) ->
        if lh > rh + 2 then bal ll lv (join lr v r)
        else if rh > lh + 2 then bal (join l v rl) rv rr
        else make l v r

let split x s =
  let rec loop x = function
    | Empty -> (Empty, false, Empty)
    | Node (l, (lk, rk), r, _, _) ->
        let c = split_cmp lk rk x in
        if c = 0 then
          if lk = x then
            if rk = x then (l, true, r)
            else (l, true, add_one (x + 1, rk) r)
          else
            if rk = x then (add_one (lk, x - 1) l, true, r)
            else (add_one (lk, x - 1) l, true, add_one (x + 1, rk) r)
        else if c < 0 then
          let (ll, pres, rl) = loop x l in (ll, pres, join rl (lk, rk) r)
        else
          let (lr, pres, rr) = loop x r in (join l (lk, rk) lr, pres, rr)
  in
  loop x s

let remove (x, y) s =
  let (l, _, _) = split x s in
  let (_, _, r) = split y s in
  let rec loop l r =
    match l, r with
    | Empty, _ -> r
    | _, Empty -> l
    | Node (ll, lv, lr, lh, _), Node (rl, rv, rr, rh, _) ->
        if lh > rh + 2 then
          bal ll lv (loop lr r)
        else
          bal (loop l rl) rv rr
  in
  loop l r

let add i s =
  let n = remove i s in
  add_one i n

let mem x s =
  let rec loop = function
    | Node (l, (lk, rk), r, _, _) ->
        let c = split_cmp lk rk x in
        c = 0 || loop (if c < 0 then l else r)
    | Empty -> false
  in
  loop s

let iter f s =
  let rec loop = function
    | Empty -> ()
    | Node (l, k, r, _, _) -> loop l; f k; loop r
  in
  loop s

let fold f s acc =
  let rec loop acc = function
    | Empty -> acc
    | Node (l, k, r, _, _) ->
        loop (f k (loop acc l)) r
  in
  loop acc s

let elements s = 
  let rec loop acc = function
    | Empty -> acc
    | Node(l, k, r, _, _) -> loop (k :: loop acc r) l
  in
  loop [] s

(* funkcja below rekurencyjnie liczy ilość elementów za pomocą wzorów:
   gdy x znajduje się w aktualnie przeglądanym przedziale:
     il elementów w lewym poddrzewie (_, _, _, _, w) + il elementów w węźle
     (pomniejszona o elementy większe od x)
   gdy wchodzi do lewego poddrzewa:
     jedynie "bierze" wartość wyliczoną dla lewego syna
   gdy wchodzi do prawego poddrzewa:
     il el w lewym poddrzewie + il el w węźle + wartość dla prawego poddrzewa *)

let below x s =
  let sum l =
    List.fold_left (fun a x ->
                     if max a x = max_int || x >= max_int - a then
                       max_int
                     else
                       a + x) 0 l
  in
  let rec loop x = function
    | Node (l, (lk, rk), r, _, _) ->
        let c = split_cmp lk rk x in
        if c = 0 then
          sum [w l; amount (lk, x)]
        else if c < 0 then
          loop x l
        else
          sum [amount (lk, rk); w l; loop x r]
    | Empty -> 0
  in
  loop x s

(*
let () = print_endline ("Test start")
let a = add (18, 18) empty
let a = add (-20, -19) a
let a = add (-6, 7) a
let b = add (-12, 17) a
let () = assert (elements b = [(-20, -19); (-12, 18)])
let b = add (9, 17) a
let () = assert (elements b = [(-20, -19); (-6, 7); (9, 18)])
let () = print_endline ("Test 1 OK")

let a = add (0, 1) empty
let a = add (5, 7) a
let a = add (2, 3) a
let a = add (-3, -2) a
let a = add (9, 10) a
let a = add (15, 16) a
let a = add (12, 13) a
let () = assert (w a = 15)
let () = assert (mem (-1) a = false)
let () = assert (mem 15 a = true)
let b = add (0, 14) a
let () = assert (elements b = [(-3, -2); (0, 16)])
let c = add (0, 9) a
let () = assert (mem 14 c = false); assert (mem 0 c = true)
let () = print_endline ("Test 2 OK")

let a = add (0, 9) empty
let a = add (-20, -10) a
let a = add (-80, -60) a
let a = add (-32, -31) a
let a = add (-24, -24) a
let a = add (-40, -35) a
let a = add (100, 120) a
let a = add (20, 30) a
let a = add (11, 15) a
let a = add (-5, -4) a
let a = add (-28, -26) a
let a = add (40, 42) a
let a = add (50, 60) a
let a = add (-2, -2) a
let a = add (-22, -22) a
let a = add (70, 80) a
let a = add (130, 140) a
let a = add (-8, -7) a
let a = add (90, 95) a
let () =
  assert (mem (-90) a = false);
  assert (mem (-80) a = true);
  assert (mem (-42) a = false);
  assert (mem (-40) a = true);
  assert (mem (-24) a = true);
  assert (mem (-21) a = false);
  assert (mem (-20) a = true);
  assert (mem 10 a = false);
  assert (mem 50 a = true);
  assert (mem 99 a = false);
  assert (mem 100 a = true);
  assert (mem 130 a = true)
let b = remove (-70, 115) a
let () =
  assert (mem (-90) b = false);
  assert (mem (-80) b = true);
  assert (mem (-42) b = false);
  assert (mem (-40) b = false);
  assert (mem (-24) b = false);
  assert (mem (-21) b = false);
  assert (mem (-20) b = false);
  assert (mem 10 b = false);
  assert (mem 50 b = false);
  assert (mem 99 b = false);
  assert (mem 100 b = false);
  assert (mem 130 b = true)
let c = remove (20, 160) a
let () =
  assert (mem (-90) c = false);
  assert (mem (-80) c = true);
  assert (mem (-42) c = false);
  assert (mem (-40) c = true);
  assert (mem (-24) c = true);
  assert (mem (-21) c = false);
  assert (mem (-20) c = true);
  assert (mem 10 c = false);
  assert (mem 50 c = false);
  assert (mem 99 c = false);
  assert (mem 100 c = false);
  assert (mem 130 c = false)
let () = print_endline ("Test 3 OK")
let () = print_endline ("\nDONE")
*)
