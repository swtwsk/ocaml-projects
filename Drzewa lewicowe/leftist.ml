(** Zadanie   : Drzewa lewicowe     **)
(** Autor     : Andrzej Swatowski   **)
(** Recenzent : Valery Karpishevich **)

(*
Kolejka to drzewo binarne o postaci
Node(lewe_poddrzewo, wartość węzła, prawe_poddrzewo, wysokość prawego poddrzewa)
*)
type 'a queue =
  | Leaf
  | Node of 'a queue * 'a * 'a queue * int

let empty = Leaf

let depth d =
  match d with
    | Leaf -> (-1)
    | Node (_, _, _, h) -> h

(* funkcja-modyfikator, łącząca dwa drzewa lewicowe *)
let rec join d1 d2 =

  (* funkcja spajająca dwa poddrzewa z korzeniem *)
  let merge left right v =
    if depth right < depth left then
      Node (left, v, right, depth right + 1)
    else
      Node (right, v, left, depth left + 1)
  in

  match d1, d2 with
    | Leaf, Leaf -> empty
    | Leaf, _ -> d2
    | _, Leaf -> d1
    | Node (l1, v1, r1, _), Node (l2, v2, r2, _) ->
        if v1 < v2 then
          merge l1 (join r1 d2) v1
        else
          merge l2 (join r2 d1) v2

let add e q =
  join (Node (Leaf, e, Leaf, 0)) q

exception Empty

let delete_min q = 
  match q with
    | Leaf -> raise Empty
    | Node (left, v, right, _) -> (v, join left right)

let is_empty q =
  match q with
    | Leaf -> true
    | _ -> false
