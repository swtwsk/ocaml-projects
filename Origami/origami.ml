(** Zadanie   : Origami          **)
(** Autor     : Andrzej Swatowski**)
(** Recenzent : Łukasz Gac       **)

type point = float * float
(** Punkt na płaszczyźnie *)

type kartka = point -> int
(** Poskładana kartka: ile razy kartkę przebije szpilka wbita w danym punkcie *)

let prostokat (p1:point) (p2:point) =
  let (x1, y1) = p1 and (x2, y2) = p2 in
  fun (x, y) ->
    if x >= x1 && x <= x2 && y >= y1 && y <= y2 then 1 else 0

let sqr x = x *. x

let kolko (p:point) r =
  let (a, b) = p in
  fun (x, y) ->
    if sqr (x -. a) +. sqr (y -. b) <= sqr r then 1 else 0

(* Funkcje pomocnicze do zloz *)
(* Iloczyn wektorowy wektorów ab i ac *)
let il_wekt a b c =
  let (ax, ay) = a and (bx, by) = b and (cx, cy) = c in
  (bx -. ax) *. (cy -. ay) -. (by -. ay) *. (cx -. ax)

(* Jeżeli wynik iloczynu jest ujemny, to punkt c znajduję się po prawej stronie
   prostej ab.
   Jeżeli dodatni, to po lewej. Jeżeli jest równy 0, to punkt leży na prostej *)
let eps = 1E-9
let strona a b c =
  let iw = il_wekt a b c in
  if iw < (-. eps) then -1 else
  if iw > eps then 1 else 0

let symetria (x1, y1) (x2, y2) (x, y) =
  if x1 -. x2 = 0. then                            (* symetria wzgl prostej x *)
    (x +. 2. *. (x1 -. x), y)
  else
    if y1 -. y2 = 0. then                          (* symetria wzgl prostej y *)
      (x, y +. 2. *. (y1 -. y))
    else
      (* prosta przechodząca przez p1 i p2 *)
      let a = (y1 -. y2) /. (x1 -. x2) in
      let b = y1 -. a *. x1 in
      (* prosta prostopadła *)
      let ap = (x2 -. x1) /. (y1 -. y2) in         (* a_prostopadłe = -1 / a) *)
      let bp = y -. ap *. x in
      (* punkt przecięcia prostych *)
      let xp = (bp -. b) /. (a -. ap) in
      let yp = a *. xp +. b in
      (x +. 2. *. (xp -. x), y +. 2. *. (yp -. y))

let zloz (p1:point) (p2:point) (k:kartka) =
  fun (x, y) ->
    let s = strona p1 p2 (x, y) in
    if s = (-1) then 0 else
      if s = 0 then k (x, y) else
        let (xs, ys) = symetria p1 p2 (x, y) in
        k (xs, ys) + k (x, y)

let skladaj (l:(point * point) list) (k:kartka) =
  fun (x, y) ->
    List.fold_left (fun a (p1, p2) -> zloz p1 p2 a) k l (x,y)

(**
(* TESTOWANIE *)
let pr = prostokat (0., 0.) (32., 34.)
let t1 = ((20., 0.), (25., 10.))
let t2 = ((25., 10.), (25., 15.))
let t3 = ((6., 25.), (6., 2.))
let t4 = ((2., 5.), (5., 5.))
let t5 = ((12., 17.), (5., 10.))
let l = [t1; t2; t3; t4; t5]

let zl = skladaj l pr
let () = assert (zl (27., 17.) = 2)
let () = assert (zl (23., 17.) = 6)
let () = assert (zl (10., 10.5) = 2)
let () = assert (zl (16., 18.) = 2)
let () = assert (zl (24., 27.) = 4)

let c = kolko (4., -1.) 7.
let () = assert (c (4., 2.) = 1)
let () = assert (c (10.04, 2.539) = 0)
let zl1 = zloz (0., -2.43) (17.01, 0.) c
let () = assert (zl1 (-2.758, -2.824) = 1)
let () = assert (zl1 (4.23, 0.) = 2)
let zl2 = zloz (6., 0.) (4., 6.) zl1
let () = assert (zl2 (4., 0.) = 4)
let () = assert (zl2 (-2., 1.) = 1)

let a = (2., 3.) and b = (5., 7.) and c = (4., 4.)
let () = assert (strona a b c = -1)
let () = assert (strona a c b = 1)
let () = assert (strona b a c = 1)
let () = assert (strona b c a = -1)

let () = assert (symetria a b c >= (2.4 -. eps, 5.2 -. eps) &&
                 symetria a b c <= (2.4 +. eps, 5.2 +. eps))
**)
