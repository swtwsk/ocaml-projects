(** Zadanie   : Arytmetyka       **)
(** Autor     : Andrzej Swatowski**)
(** Recenzent : Paweł Brzeziński **)

(** T Y P Y **)
type wartosc = 
    | Interval of float * float
    | Union of wartosc * wartosc

(* wartosc to albo przedział (float * float),
   albo Suma przedziałów, wyrażona poprzez Union (Interval, Interval) *)
(* Interval (0.3, 4.) oznacza przedział [0.3 ; 4] *)
(* Union (Interval (neg_infinity, (-2.)), Interval (2., infinity))
   oznacza przedział (neg_inf ; -2] u [2 ; inf) *)

(** K O N S T R U K T O R Y **)
let wartosc_dokladnosc x p =
  let dx = (abs_float x) *. (p /. 100.) in
  Interval (x -. dx, x +. dx)

let wartosc_od_do x y = Interval (x, y)
let wartosc_dokladna x = Interval (x, x)

(** S E L E K T O R Y **)
let rec in_wartosc p x =
  match p with
    | Interval (a, b) -> a <= x && x <= b
    | Union (a, b) -> in_wartosc a x || in_wartosc b x

let min_wartosc p =
  let rec minimum prz =
    match prz with
      | Interval (a, _) -> a
      | Union (a, b) -> min (minimum a) (minimum b)
  in
    minimum p

let max_wartosc p =
  let rec maksimum prz =
    match prz with
      | Interval (_, b) -> b
      | Union (a, b) -> max (maksimum a) (maksimum b)
  in
    maksimum p

let sr_wartosc p =
  (min_wartosc p +. max_wartosc p) /. 2.

(** M O D Y F I K A T O R Y **)

(* join służy do łączenia przedziałów :
   jeżeli dwa przedziały a i b przecinają się,
   to ich suma zostaje zamieniona na pojedynczy przedział *)
let join x =
  let join_pom a b =
    match a, b with
      | Interval (a1, a2), Interval (b1, b2) ->
          if a1 <= b1 then
            if a2 <= b1 then
              if a2 = b1 then
                Interval (a1, b2)
              else
                Union (a, b)
            else
              if a2 <= b2 then
                Interval (a1, b2)
              else
                Interval (a1, a2)
          else
            if a1 <= b2 then
              if a2 <= b2 then
                Interval (b1, b2)
              else
                Interval (b1, a2)
            else
              if a1 = b2 then
                Interval (b1, a2)
              else
                Union (b, a)
      | _, _ -> Union (a, b)
  in
    match x with
      | Union (a, b) -> join_pom a b
      | Interval (a, b) ->
          if a > b then Interval (b, a) else Interval (a, b)

(* PLUS *)
let plus x y =
  let plus_pom x y =
    match x, y with
      | Interval (a, b), Interval (c, d) ->
          Interval (a +. c, b +. d)
      | Interval (a, b), Union (Interval (c1, c2), Interval (d1, d2)) ->
          Union (Interval (c1 +. a, c2 +. b), Interval (d1 +. a, d2 +. b))
      | Union (Interval (a1, a2), Interval (b1, b2)), Interval (c, d) ->
          Union (Interval (a1 +. c, a2 +. d), Interval (b1 +. c, b2 +. d))
      | Union (_, _), Union (_, _) ->
          Interval (neg_infinity, infinity)
      | _, _ -> failwith "Blad plusa!"
  in
    join (plus_pom x y)

(* MINUS : x - y = x + (-y) *)
let minus x y =
  let a = Interval (min_wartosc x, max_wartosc x) in
  let b = Interval ((-1.) *. max_wartosc y, (-1.) *. min_wartosc y) in
  plus a b

let rec min_f l =
  match l with
    | [] -> infinity
    | h::t -> min h (min_f t)
let rec max_f l =
  match l with
    | [] -> neg_infinity
    | h::t -> max h (max_f t)

(* RAZY *)
let razy p q =
  let rec razy_pom x y =

    let l =
      let a = min_wartosc x and b = max_wartosc x in
      let c = min_wartosc y and d = max_wartosc y in
      [a *. c; a *. d; b *. c; b *. d]
    in

    match x, y with
      | Interval (a, b), Interval (c, d) ->
          let czy_nan x = (compare nan x = 0)
          in
            if czy_nan a || czy_nan b || czy_nan c || czy_nan d then
              Interval (nan, nan)
            else
              if (c = 0. && d = 0.) || (a = 0. && b = 0.) then
                Interval (0., 0.)
              else
                Interval (min_f l, max_f l)
        | Union (a, b), Interval (_, _) ->
            Union (razy_pom a y, razy_pom b y)
        | Interval (_, _), Union (c, d) ->
            Union (razy_pom x c, razy_pom x d)
        | Union (a, b), Union (_, _) ->
            Union (join (razy_pom a y), join (razy_pom b y))

  in join (razy_pom p q)

(* DZIELENIE *)
let podzielic p q =
  let rec podzielic_pom x y =

    let odwr a = 1. /. a in

    match x, y with
      | Interval (a, b), Interval (c, d) ->
          if c = 0. && d = 0. then        (* dzielenie przez singleton {0} *)
            Interval (nan, nan)           (* zwraca przedział pusty        *)
          else if c = 0. then
            razy x (Interval ((odwr d), infinity))
          else if d = 0. then
            razy x (Interval (neg_infinity, (odwr c)))
          else
            if in_wartosc y 0. then
              let a = Interval (neg_infinity, (odwr c)) in
              let b = Interval ((odwr d), infinity) in
              razy x (Union (a, b))
            else
              razy x (Interval (odwr d, odwr c))
      | Union (a, b), Interval (_, _) ->
          Union (podzielic_pom a y, podzielic_pom b y)
      | Interval (_, _), Union (c, d) ->
          Union (podzielic_pom x c, podzielic_pom x d)
      | Union (a, b), Union (_, _) ->
          Union (join (podzielic_pom a y), join (podzielic_pom b y))

  in
    join (podzielic_pom p q)

(* operator = *)
let epsilon = 0.0001
let (=.) x y = (x -. epsilon <= y) && (y <= x +. epsilon)
